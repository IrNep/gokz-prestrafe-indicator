#include <sourcemod>

#include <gokz/core>
#include <gokz/replays>
#include <gokz/kzplayer>
#include <gokz/kztpreindicator>

#pragma newdecls required
#pragma semicolon 1

bool gB_GOKZReplays;

#include "gokz-kzt-pre-indicator/options.sp"
#include "gokz-kzt-pre-indicator/pre.sp"
#include "gokz-kzt-pre-indicator/hud.sp"
#include "gokz-kzt-pre-indicator/commands.sp"

public Plugin myinfo = 
{
	name = "GOKZ KZTimer Prestrafe Indicator", 
	author = "Past", 
	description = "Shows KZTimer players' PreVelMod & PreTickCounter.", 
	version = "1.1", 
	url = ""
};

public void OnPluginStart()
{
	LoadTranslations("gokz-kzt-pre-indicator.phrases");
	gB_GOKZReplays = LibraryExists("gokz-replays");
	PrepareHUD();
	RegisterCommands();
}

public void OnAllPluginsLoaded()
{
	gB_GOKZReplays = LibraryExists("gokz-replays");
	TopMenu topMenu;
	if (LibraryExists("gokz-core") && (topMenu = GOKZ_GetOptionsTopMenu()) != null)
	{
		GOKZ_OnOptionsMenuReady(topMenu);
	}
}

public void GOKZ_OnOptionsMenuReady(TopMenu topMenu)
{
	RegisterOptions();
}

public void GOKZ_OnCountedTeleport_Post(int client)
{
	ResetPrestrafeVelMod(client);
}

public void GOKZ_OnOptionChanged(int client, const char[] option, any newValue)
{
	any pOption;
	if(GOKZ_PreIndicator_IsPreIndicatorOption(option, pOption))
	{
		OnOptionChanged(client, pOption, newValue);
	}
}

public void OnPlayerRunCmdPost(int client, int buttons, int impulse, const float vel[3], const float angles[3], int weapon, int subtype, int cmdnum, int tickcount, int seed, const int mouse[2])
{
	UpdatePreIndicator(client);
}


// Preserve for health base map checking.
// mentioned by zea0.k
bool IsHealthMap()
{
	return false;
}