## Description:

 * 将KZT玩家的地速状态可视化
 * Show KZT players prestrafe status. 

## Dependency: 

 * [GOKZ](https://bitbucket.org/kztimerglobalteam/gokz/downloads/)

## How To Use:

 * Command: 
    * !pre -- Switch indicator style 切换可视化模式
 * Options:
    * !options -> HUD -> Prestrafe Indicator 地速指示器

## Indicator Styles:

 * Health And Armor 血量与护甲
    * Health indicates Prestrafe Percentage 血量代表地速大小
    * Armor indicates Prestrafe Tick Counter 护甲代表地速Tick
    * This style only work for self and spectators options wont effect players 该模式仅影响自身和正在观察自己的观察者
  
 * Bottom 中心下方
    * First line indicates max prestrafe speed that player can reach 第一行代表地速大小
    * Second line indicates Prestrafe Tick Counter with brackets 第二行代表地速Tick
    * Text Turn red when Tick Counter reach 73 indicate prestrafe decreasing 地速进入下降阶段时两值变红
    * This style work for any kzt players including replay bots 该模式可在观察KZT玩家/BOT时生效


## Restriction:
 * Work on kzt *only*. *仅*在KZT模式生效

 * Health style **won't** work on maps that use fall damage in the future(waiting for gokz api).
